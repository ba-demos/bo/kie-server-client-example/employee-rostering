Employee Rostering Example
==================================================

Business Optimizer Project.

Check the companion project that exploit this solution: `kie-client`

Pay attention to the `LocalDateTime` annotation in the `employeerostering.employeerostering.Timeslot`:

```java
@JsonSerialize(using=LocalDateTimeSerializer.class)
@JsonDeserialize(using=LocalDateTimeDeserializer.class)
@JsonFormat(shape = Shape.STRING)
private java.time.LocalDateTime startTime;
```

Solver configuration
--------------------------------------------------

In this project there are 2 solver configurations:

- [Employee_Rostering Normal](src/main/resources/employeerostering/employeerostering/EmployeeRosteringSolverConfig.solver.xml)
- [Employee_Rostering Test](src/main/resources/employeerostering/test/EmployeeRosteringSolverConfig.solver.test.xml)

They point to two different ksession configuration:

```xml
<scoreDirectorFactory xStreamId="3">
    <ksessionName>ksession</ksessionName>
</scoreDirectorFactory>
```

Kie sessions are defined in the [kmodule.xml](src/main/resources/META-INF/kmodule.xml):

```xml
<kbase name="kbase" default="true" packages="employeerostering.employeerostering">
    <ksession name="ksession" type="stateful" default="true" clockType="realtime"/>
</kbase>
<kbase name="kbase-test" default="false" packages="employeerostering.*">
    <ksession name="ksession-test" type="stateful" default="true" clockType="realtime"/>
</kbase>
```

The kie session belong to a specific kie base where it's possible to define the packages to include in the knowledge base.
In this example, `ksession` uses all the rules belonging to the package `employeerostering.employeerostering`, whereas the `ksession-test` uses all the rules contained in the packages `employeerostering.*`, then even the rule in `employeerostering.test` package.

`packages` can be a comma separated list of resource packages to be included in the kbase.

Note that the rule package is defined in the `.drl` file:

```java
package employeerostering.test;
```