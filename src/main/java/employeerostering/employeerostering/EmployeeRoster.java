/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package employeerostering.employeerostering;

import java.util.List;
import java.util.stream.Collectors;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@org.optaplanner.core.api.domain.solution.PlanningSolution(autoDiscoverMemberType = org.optaplanner.core.api.domain.autodiscover.AutoDiscoverMemberType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class EmployeeRoster implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    @ValueRangeProvider(id = "employeeRange")
    @ProblemFactCollectionProperty
    private List<employeerostering.employeerostering.Employee> employeeList;
    private List<employeerostering.employeerostering.Shift> shiftList;
    private List<employeerostering.employeerostering.Skill> skillList;
    private List<employeerostering.employeerostering.Timeslot> timeslotList;
    private List<employeerostering.employeerostering.DayOffRequest> dayOffRequestList;
    @PlanningEntityCollectionProperty
    private List<employeerostering.employeerostering.ShiftAssignment> shiftAssignmentList;

    @org.kie.api.definition.type.Label("Generated Planner score field")
    @javax.annotation.Generated({"org.optaplanner.workbench.screens.domaineditor.client.widgets.planner.PlannerDataObjectEditor"})
    @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(org.optaplanner.persistence.jaxb.api.score.buildin.hardsoft.HardSoftScoreJaxbXmlAdapter.class)
    @PlanningScore
    private org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore score;

    public EmployeeRoster() {
    }

    public List<employeerostering.employeerostering.Employee> getEmployeeList() {
        return this.employeeList;
    }

    public void setEmployeeList(
                                List<employeerostering.employeerostering.Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<employeerostering.employeerostering.Shift> getShiftList() {
        return this.shiftList;
    }

    public void setShiftList(
                             List<employeerostering.employeerostering.Shift> shiftList) {
        this.shiftList = shiftList;
    }

    public List<employeerostering.employeerostering.Skill> getSkillList() {
        return this.skillList;
    }

    public void setSkillList(
                             List<employeerostering.employeerostering.Skill> skillList) {
        this.skillList = skillList;
    }

    public List<employeerostering.employeerostering.Timeslot> getTimeslotList() {
        return this.timeslotList;
    }

    public void setTimeslotList(
                                List<employeerostering.employeerostering.Timeslot> timeslotList) {
        this.timeslotList = timeslotList;
    }

    public List<employeerostering.employeerostering.ShiftAssignment> getShiftAssignmentList() {
        return this.shiftAssignmentList;
    }

    public void setShiftAssignmentList(
                                       List<employeerostering.employeerostering.ShiftAssignment> shiftAssignmentList) {
        this.shiftAssignmentList = shiftAssignmentList;
    }

    public List<employeerostering.employeerostering.DayOffRequest> getDayOffRequestList() {
        return this.dayOffRequestList;
    }

    public void setDayOffRequestList(
                                     List<employeerostering.employeerostering.DayOffRequest> dayOffRequestList) {
        this.dayOffRequestList = dayOffRequestList;
    }

    public HardSoftScore getScore() {
        return score;
    }

    public void setScore(
                         org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore score) {
        this.score = score;
    }

    public EmployeeRoster(
                          List<employeerostering.employeerostering.Employee> employeeList,
                          List<employeerostering.employeerostering.Shift> shiftList,
                          List<employeerostering.employeerostering.Skill> skillList,
                          List<employeerostering.employeerostering.Timeslot> timeslotList,
                          List<employeerostering.employeerostering.DayOffRequest> dayOffRequestList,
                          org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore score) {
        this.employeeList = employeeList;
        this.shiftList = shiftList;
        this.skillList = skillList;
        this.timeslotList = timeslotList;
        this.dayOffRequestList = dayOffRequestList;
        this.shiftAssignmentList = shiftList.stream().map(s -> new ShiftAssignment(s, null)).collect(Collectors.toList());
        this.score = score;
    }

}
